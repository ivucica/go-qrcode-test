package main

import (
	qrcode "github.com/skip2/go-qrcode"

	"flag"
	"log"
	"net/http"
	"strings"
)

var (
	address = flag.String("address", ":8080", "listen address")
)

func handler(w http.ResponseWriter, r *http.Request) {

	if r.RequestURI == "/" {
		w.Header().Set("content-type", "text/html")
		w.Write([]byte("<a href=\"/otpauth://totp/ACME%20Co:john.doe@email.com?secret=HXDMVJECJJWSRB3HWIZR4IFUGFTMXBOZ&issuer=ACME%20Co&algorithm=SHA1&digits=6&period=30\">totp</a><br>\n"))
		w.Write([]byte("<a href=\"/OPENPGP4FPR:192FB8E872B54B175FB86EBF9A3B99C88F9E89D5\">openpgp fpr</a><br>\n"))
		return
	}

	var png []byte
	png, err := qrcode.Encode(strings.Replace(strings.Replace(strings.Replace(r.RequestURI[1:], "otpauth:/totp", "otpauth://totp", -1), "http:/", "http://", -1), "http:///", "http://", -1), qrcode.Medium, 256)
	if err != nil {
		w.Header().Set("content-type", "text/html")
		w.Write([]byte("err: " + err.Error()))
		return
	}

	w.Header().Set("Content-type", "image/png")
	w.Write(png)
}

func main() {
	flag.Parse()

	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(*address, nil))
}
